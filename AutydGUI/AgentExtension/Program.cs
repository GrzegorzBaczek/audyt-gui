﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using b32p13k4.ZabbixApi;

namespace AgentExtension
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var ip = IPAddress.Loopback;
                ushort port = 45678;
                var client = new Client(ip, port);
                Task.Run(async () =>
                {
                    try
                    {
                        await client.Start();
                    }
                    catch (IOException)
                    {
                        Console.WriteLine("Server closed the connection.");
                    }
                }).Wait();
            }
            catch (Exception)
            {
                Console.WriteLine("An error occured.\n Try again later.\n");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
