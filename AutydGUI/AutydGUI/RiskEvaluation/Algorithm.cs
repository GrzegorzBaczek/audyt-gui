﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutydGUI.Models;

namespace AutydGUI.RiskEvalutaion
{
    class Algorithm
    {
        private Matrices MatricesData = null;
        private List<Scenario> ScenarioList = null;
        private List<Scenario> ScenarioListTemplate = null;
        private List<SubService> ScenarioAnswers = null;

        public Algorithm()
        {
            MatricesData = new Matrices();
        }

        public void CalculateScenarioData(List<SubService> Answers, List<Scenario> Scenarios)
        {
            ScenarioAnswers = new List<SubService>(Answers);
            ScenarioList = new List<Scenario>(Scenarios);
            if (ScenarioListTemplate == null)
            {
                ScenarioListTemplate = new List<Scenario>(Scenarios.ConvertAll(Scenario.Copy));
            }
            Calculate();
        }

        public List<Scenario> GetScenarioData()
        {
            return ScenarioList;
        }

        private void Calculate()
        {
            if (ScenarioAnswers == null || ScenarioList == null)
            {
                Console.WriteLine("There is no scenarios loaded!");
            }
            else
            {
                for (int i = 0; i < ScenarioList.Count; i++)
                {                   
                    ScenarioList[i].DisruptiveActions = CalculateScenarioSection(ScenarioListTemplate[i].DisruptiveActions).ToString();
                    ScenarioList[i].PreventiveActions = CalculateScenarioSection(ScenarioListTemplate[i].PreventiveActions).ToString();
                    ScenarioList[i].ProtectiveActions = CalculateScenarioSection(ScenarioListTemplate[i].ProtectiveActions).ToString();
                    ScenarioList[i].SoothingActions = CalculateScenarioSection(ScenarioListTemplate[i].SoothingActions).ToString();
                    ScenarioList[i].ReproductiveActions = CalculateScenarioSection(ScenarioListTemplate[i].ReproductiveActions).ToString();

                    ScenarioList[i].Probability = CalculateProbability(ScenarioList[i].CauseCriteria, ScenarioList[i].Resource, ScenarioList[i].DisruptiveActions, ScenarioList[i].PreventiveActions);
                    ScenarioList[i].EstimatedConsequence = CalculateEstimatedConsequence(ScenarioList[i].ConsequenceCriteria, ScenarioList[i].ProtectiveActions, ScenarioList[i].ReproductiveActions, ScenarioList[i].SoothingActions);

                    ScenarioList[i].Consequence = CalculateConsequence(ScenarioList[i].Resource, ScenarioList[i].EstimatedConsequence);
                    ScenarioList[i].Risk = CalculateRisk(ScenarioList[i].Consequence, ScenarioList[i].Probability);
                }
            }
        }

        private int CalculateRisk(int Consequence, int Probability)
        {
            return MatricesData.GetValue("Risk", 1, Consequence, Probability);
        }

        private int CalculateConsequence(string Resource, int EstimatedConsequence)
        {
            return MatricesData.GetValue("Consequence", 1, Int32.Parse(Resource), EstimatedConsequence);
        }

        /// <summary>
        /// Calculates probability
        /// </summary>
        /// <param name="CauseCriteria">Name of scenario cause criteria</param>
        /// <param name="Resource">Value of scenario resource</param>
        /// <param name="XValue">Value of scenario disruptive actions</param>
        /// <param name="YValue">Value of scenario preventing acitons</param>
        /// <returns>Probability value</returns>
        private int CalculateProbability(string CauseCriteria, string Resource, string XValue, string YValue)
        {
            string CauseCriteriaName = null;

            if (CauseCriteria.Equals("Wypadki / Zdarzenia losowe"))
                CauseCriteriaName = "Accident";
            else if (CauseCriteria.Equals("Błąd"))
                CauseCriteriaName = "Fault";
            else if (CauseCriteria.Equals("Działania Celowe"))
                CauseCriteriaName = "Purpose";

            return MatricesData.GetValue(CauseCriteriaName, Int32.Parse(Resource), Int32.Parse(XValue), Int32.Parse(YValue));
        }

        /// <summary>
        /// Calcualtes estimated Consequence
        /// </summary>
        /// <param name="ConsequenceCriteria">Name of consequence criteria</param>
        /// <param name="ProtectiveValue">Value of scenario protective actions</param>
        /// <param name="XValue">Value of scenario reproductive actions</param>
        /// <param name="YValue">Value of scenario soothing actions</param>
        /// <returns>Estimated Consequence value</returns>
        private int CalculateEstimatedConsequence(string ConsequenceCriteria, string ProtectiveValue, string XValue, string YValue)
        {
            string ConsequenceCriteriaName = null;

            if (ConsequenceCriteria.Equals("Dostępność"))
                ConsequenceCriteriaName = "Availability";
            else if (ConsequenceCriteria.Equals("Integralność"))
                ConsequenceCriteriaName = "Integrality";
            else if (ConsequenceCriteria.Equals("Poufność"))
                ConsequenceCriteriaName = "Confidentiality";

            return MatricesData.GetValue(ConsequenceCriteriaName, Int32.Parse(ProtectiveValue), Int32.Parse(XValue), Int32.Parse(YValue));
        }

        private int CalculateMinMax(string Operator, int LeftValue, int RightValue)
        {
            if (Operator.Equals("MIN"))
                return (LeftValue < RightValue) ? LeftValue : RightValue;
            else
                return (LeftValue > RightValue) ? LeftValue : RightValue;            
        }

        private string GetLeftArg(string Arg, ref int SemicolonIndex)
        {
            bool CanSkip = false;
            int LeftIndex = 0, RightIndex = 0;
            string Tmp = Arg.Substring(LeftIndex, 3);

            while (!(Tmp.Substring(2, 1).Equals(";") && !CanSkip))
            {
                if (Tmp.Substring(2, 1).Equals(";") && CanSkip)
                    CanSkip = false;

                if (Tmp.Equals("MIN") || Tmp.Equals("MAX"))
                {
                    CanSkip = true;
                    LeftIndex += 4;
                }
                else
                {
                    LeftIndex += 1;                    
                }
                RightIndex = LeftIndex + 3;
                Tmp = Arg.Substring(LeftIndex, 3);
            }
            SemicolonIndex += RightIndex;

            return Arg.Substring(0, RightIndex - 1);
        }

        private int CalculateScenarioSection(string Arg)
        {
            if (Arg == null || Arg.Equals(""))
            {
                return 1;
            }

            string Operator = Arg.Substring(0, 3);
            string LeftArg;
            int SemicolonIndex = 4;
            int LeftArgValue, RightArgValue;
            
            if (Operator.Equals("MIN") || Operator.Equals("MAX"))
            {
                LeftArg = GetLeftArg(Arg.Substring(4, Arg.Length - 4), ref SemicolonIndex);
                LeftArgValue = CalculateScenarioSection(LeftArg);

                RightArgValue = CalculateScenarioSection(Arg.Substring(SemicolonIndex, Arg.Length - SemicolonIndex - 1));

                return CalculateMinMax(Operator, LeftArgValue, RightArgValue);
            }
            else 
            {
                return CalculateAnswersWage(Arg);
            }
        }

        private int CalculateAnswersWage(String ScenarioID)
        {
            int MaxValue = 1000, MinValue = 0, Wage = 0, Result = 0;
            double Answer = 0;
            bool IsMin = false, IsMax = false;

            foreach (var Scenario in ScenarioAnswers.Where(Scenario => Scenario.ID == ScenarioID))
            {
                foreach (var Question in Scenario.ListOfQuestions)
                {
                    Wage += Int32.Parse(Question.Wage);

                    if (Question.QuestionAnswer == Models.Answer.Tak)
                    {
                        Answer += Int32.Parse(Question.Wage);
                        if (Question.PMin != null && !Question.PMin.Equals("") && Int32.Parse(Question.PMin) > MinValue)
                        {
                            MinValue = Int32.Parse(Question.PMin);
                            IsMin = true;
                        }
                    }
                    else if (Question.QuestionAnswer == Models.Answer.Nie)
                    {
                        if (Question.PMax != null && !Question.PMax.Equals("") && Int32.Parse(Question.PMax) < MaxValue)
                        {
                            MaxValue = Int32.Parse(Question.PMax);
                            IsMax = true;
                        }
                    }
                    else
                        Console.WriteLine("Check your scenario answers data! The answer is not included in the range of [YES/NO].");
                }
                if (Answer > 0)
                    Scenario.averageValue = 4 * (Answer / Wage);               
            }
            if (Answer > 0)
                Answer = 4 * (Answer / Wage);

            

            if (Answer <= 1.5)
                Result = 1;
            if (Answer <= 2.5)
                Result = 2;
            if (Answer <= 3.5)
                Result = 3;
            if (Answer > 3.5)
                Result = 4;

            if (IsMin && Result < MinValue)
                Result = MinValue;
            if (IsMax && Result > MaxValue)
                Result = MaxValue;

            return Result;
        }
    }
}
