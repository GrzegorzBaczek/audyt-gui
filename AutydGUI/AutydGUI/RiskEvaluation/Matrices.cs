﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.RiskEvalutaion
{
    class Matrices
    {
        private Dictionary<string, List<byte[,]>> dictionary = null;

        public Matrices()
        {
            dictionary = new Dictionary<string, List<byte[,]>>();

            dictionary.Add("Accident", new List<byte[,]>());
            dictionary.Add("Fault", new List<byte[,]>());
            dictionary.Add("Purpose", new List<byte[,]>());
            dictionary.Add("Availability", new List<byte[,]>());
            dictionary.Add("Integrality", new List<byte[,]>());
            dictionary.Add("Confidentiality", new List<byte[,]>());
            dictionary.Add("Consequence", new List<byte[,]>());
            dictionary.Add("Risk", new List<byte[,]>());

            dictionary["Accident"].Add(new byte[,] { { 1, 1, 1, 1 } });
            dictionary["Accident"].Add(new byte[,] { { 2, 2, 2, 1 } });
            dictionary["Accident"].Add(new byte[,] { { 3, 3, 2, 1 } });
            dictionary["Accident"].Add(new byte[,] { { 4, 4, 2, 1 } });

            dictionary["Fault"].Add(new byte[,] { { 1, 1, 1, 1 } });
            dictionary["Fault"].Add(new byte[,] { { 2, 2, 2, 1 } });
            dictionary["Fault"].Add(new byte[,] { { 3, 3, 2, 1 } });
            dictionary["Fault"].Add(new byte[,] { { 4, 4, 2, 1 } });

            dictionary["Purpose"].Add(new byte[,] { { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 } });
            dictionary["Purpose"].Add(new byte[,] { { 2, 2, 2, 1 }, { 2, 2, 2, 1 }, { 2, 2, 1, 1 }, { 1, 1, 1, 1 } });
            dictionary["Purpose"].Add(new byte[,] { { 3, 3, 2, 1 }, { 3, 3, 2, 1 }, { 2, 2, 1, 1 }, { 2, 2, 1, 1 } });
            dictionary["Purpose"].Add(new byte[,] { { 4, 4, 3, 2 }, { 4, 4, 3, 2 }, { 3, 3, 2, 2 }, { 2, 2, 2, 1 } });

            dictionary["Availability"].Add(new byte[,] { { 4, 3, 3, 3 }, { 4, 3, 3, 3 }, { 4, 3, 3, 2 }, { 4, 3, 2, 2 } });
            dictionary["Availability"].Add(new byte[,] { { 3, 3, 3, 3 }, { 3, 3, 3, 2 }, { 3, 3, 2, 1 }, { 3, 3, 2, 1 } });
            dictionary["Availability"].Add(new byte[,] { { 3, 3, 3, 3 }, { 3, 3, 2, 2 }, { 3, 3, 2, 1 }, { 3, 3, 2, 1 } });
            dictionary["Availability"].Add(new byte[,] { { 3, 3, 3, 3 }, { 3, 3, 2, 2 }, { 3, 3, 2, 1 }, { 3, 3, 2, 1 } });

            dictionary["Integrality"].Add(new byte[,] { { 4, 3, 3, 3 }, { 4, 3, 2, 2 }, { 4, 3, 2, 1 }, { 4, 3, 2, 1 } });
            dictionary["Integrality"].Add(new byte[,] { { 3, 3, 3, 3 }, { 2, 2, 2, 2 }, { 2, 2, 1, 1 }, { 2, 2, 1, 1 } });
            dictionary["Integrality"].Add(new byte[,] { { 3, 3, 3, 3 }, { 2, 2, 2, 2 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 } });
            dictionary["Integrality"].Add(new byte[,] { { 3, 3, 3, 3 }, { 2, 2, 2, 2 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 } });

            dictionary["Confidentiality"].Add(new byte[,] { { 4 }, { 4 }, { 3 }, { 3 } });
            dictionary["Confidentiality"].Add(new byte[,] { { 3 }, { 3 }, { 3 }, { 2 } });
            dictionary["Confidentiality"].Add(new byte[,] { { 3 }, { 3 }, { 2 }, { 2 } });
            dictionary["Confidentiality"].Add(new byte[,] { { 3 }, { 2 }, { 1 }, { 1 } });

            dictionary["Consequence"].Add(new byte[,] { { 1, 1, 1, 1 }, { 1, 2, 2, 2 }, { 1, 2, 3, 3 }, { 1, 2, 3, 4 } });

            dictionary["Risk"].Add(new byte[,] { { 1, 1, 1, 2 }, { 1, 2, 2, 3 }, { 2, 3, 3, 4 }, { 2, 3, 4, 4 } });
        }

        public byte GetValue(string ListName, int MatrixNumber, int MatrixX, int MatrixY)
        {
            return dictionary[ListName].ElementAt(MatrixNumber - 1)[MatrixX - 1, MatrixY - 1];
        }
    }
}
