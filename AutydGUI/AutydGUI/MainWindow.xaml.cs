﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WpfCharts;
using System.Collections.ObjectModel;
using AutydGUI.Models;
using AutydGUI.RiskEvalutaion;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Input;
using b32p13k4.ZabbixApi;
using Evirth.Zabbix;
using System.Windows.Controls.DataVisualization.Charting;
using Evirth.Zabbix.Enums;

namespace AutydGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public string[] Values_Chart_Axes { get; set; }
        public string[] Charts_Chart_Axes { get; set; }
        public ObservableCollection<ChartLine> Values_Chart_Lines { get; set; }
        public string[] Risk_Chart_Axes { get; set; }
        public ObservableCollection<ChartLine> Charts_Chart_Lines { get; set; }
        public ObservableCollection<ChartLine> Risk_Chart_Lines { get; set; }
        public ObservableCollection<ChartLine> Values_Lines { get; set; }
        public ObservableCollection<Values_GridView> _Values_GridView { get; set; }
        public ObservableCollection<Values_Availability> _Values_Availability { get; set; }
        public ObservableCollection<Values_ServiceImprovement> _Values_ServiceImprovement { get; set; }
        public ObservableCollection<Values_RiskAssessment> _Values_RiskAssessment { get; set; } //= new ObservableCollection<Values_RiskAssessment>();
        public Server ExtServer { get; set; }
        public static IZabbixClient ZabbixClient { get; set; }
        public string ElapsedTime { get; set; }

        // don't ask
        private bool RiskAssessment_ReloadGrid = false;


        private void GUI_ReloadData_Button_Click(object sender, RoutedEventArgs e)
        {
            _stopwatch = Stopwatch.StartNew();
            CopyListsToOldOnes();
            try
            {
                var firstInfo = ExtServer.GetInfo().FirstOrDefault();
                if (firstInfo != null)
                {
                    var result = Converter.ConvertJsonConfig(firstInfo);

                    foreach (var item in result)
                    {
                        var question = ListOfQuestions.Find(x => x.ID == item.Key);
                        if (ZabbixClient?.Status == Status.Connected)
                        {
                            switch (question.ID)
                            {
                                case "05A01-05":
                                    question.QuestionAnswer = ZabbixClient.IsPublicFirewallOn() ? Answer.Tak : Answer.Nie;
                                    break;
                                case "11D06-02":
                                    question.QuestionAnswer = ZabbixClient.IsRealTimeProtectionOn() ? Answer.Tak : Answer.Nie;
                                    break;
                                default:
                                    question.QuestionAnswer = item.Value.Equals("tak") ? Answer.Tak : Answer.Nie;
                                    break;
                            }
                        }
                        else
                        {
                            question.QuestionAnswer = item.Value.Equals("tak") ? Answer.Tak : Answer.Nie;
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Could not get the values");
            }
            finally
            {
                _stopwatch.Stop();
                ElapsedTime = _stopwatch.Elapsed.TotalMilliseconds.ToString("F2") + "ms";
                NotifyPropertyChanged("ElapsedTime");
            }
            algorithm.CalculateScenarioData(ListOfSubServices, ScenarioList);
        }

        private void GUI_RecalculateData_Button_Click(object sender, RoutedEventArgs e)
        {
            CopyListsToOldOnes();
            algorithm.CalculateScenarioData(ListOfSubServices, ScenarioList);
        }

        private void CopyListsToOldOnes()
        {
            OldListOfMainClass = new List<Domain>(ListOfMainClass.ConvertAll(Domain.Copy));
            OldListOfSubServices = new List<SubService>(ListOfSubServices.ConvertAll(SubService.Copy));
            OldListOfQuestions = new List<Questions>(ListOfQuestions.ConvertAll(Questions.Copy));

            OldScenarioList = new List<Scenario>(ScenarioList.ConvertAll(Scenario.Copy));
            OldDangerDict = new Dictionary<string, string>(DangerDict);
            OldDangerList = new List<Danger>(DangerList.ConvertAll(Danger.Copy));
            OldResourcesDict = new Dictionary<string, string>(ResourcesDict);

            RiskAssessment_ReloadGrid = true;
        }

        #region Audit_Tab_Globals

        public Models.Questions CurrentQuestion = null;
        private List<Models.Domain> ListOfMainClass = null;
        private List<Models.SubService> ListOfSubServices = null;
        private List<Models.Questions> ListOfQuestions = null;

        private List<Models.Domain> OldListOfMainClass = null;
        private List<Models.SubService> OldListOfSubServices = null;
        private List<Models.Questions> OldListOfQuestions = null;

        private Stopwatch _stopwatch;

        #endregion Audit_Tab_Globals

        #region RiskCalc_Globals

        private Algorithm algorithm = null;
        private List<Models.Scenario> ScenarioList = null;
        private List<Scenario2> Scenario2List = null;
        private Dictionary<string, string> DangerDict = null;
        private List<Models.Danger> DangerList = null;
        private Dictionary<string, string> ResourcesDict = null;
        private List<Models.ResourcesType> ResourcesList = null;

        private List<Models.Scenario> OldScenarioList = null;
        private Dictionary<string, string> OldDangerDict = null;
        private List<Models.Danger> OldDangerList = null;
        private Dictionary<string, string> OldResourcesDict = null;

        #endregion RiskCalc_Globals

        #region Resources_Tab_Globals 
        public Models.Resources CurrentResource;
        #endregion

        private readonly IPAddress _ip = IPAddress.Loopback;
        private readonly ushort _port = 45678;

        public MainWindow()
        {
            InitializeComponent();
            algorithm = new Algorithm();

            _Values_RiskAssessment = new ObservableCollection<Values_RiskAssessment>();
            // Audit_Tab_Init
            InitializeListOfMainClass();

            if (ListOfMainClass != null)
            {
                InitializeTreeViewForAudit();
                InitializeGUI_Audit_DomainsTree();
            }

            GUI_Audit_Slider.Maximum = ListOfQuestions.Count - 1;

            if (ListOfQuestions.Count > 0)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView(0);
            }

            // RiskCalc_Init
            InitializeDanger();
            InitializeResorces();
            InitializeScenarioList();
            InitializeScenario2List();

            InitializeGUI_NaturalDanger_Tree();

            algorithm.CalculateScenarioData(ListOfSubServices, ScenarioList);
            CopyListsToOldOnes();

            InitializeGUI_RiskAssessment_ScenarioTree();
            InitializeGUI_RiskAssessment_WeightGrid();

            SpecialActionsInit();

            #region ResourcesTab_Init 

            InitializeTreeViewForResources(ResourcesList);
            //    InitializeScenarioTreeViewForResources(ScenarioList); 
            #endregion

            // Start AgentExt server
            ExtServer = new Server(_ip, _port);
            var t = new Thread(ExtServer.Start)
            {
                IsBackground = true
            };
            t.Start();

            InitializeTreeViewForCharts(ListOfMainClass);
            InitializeTreeViewForValues();
            ScenarioInit();
        }

        #region Audit_Tab

        private void InitializeListOfMainClass()
        {
            try
            {
                // Reading file to string matrix
                StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/AUDYT.CSV", Encoding.UTF8);  //hardcoding FTW
                string input = SR.ReadToEnd();
                string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { ';' }, StringSplitOptions.None)).ToArray();

                // Lists initializing
                ListOfMainClass = new List<Domain>();
                ListOfSubServices = new List<SubService>();
                ListOfQuestions = new List<Questions>();

                // Creating List
                int mainClassIndex = 0;
                int subClassIndex = 0;
                int subSubClassIndex = 0;
                int questionIndex = 0;

                for (int i = 4; i < linesMatrix.GetLength(0); ++i)
                {
                    if (linesMatrix[i].Length == 8)
                    {
                        if (!string.IsNullOrEmpty(linesMatrix[i][0])) // This is Domain
                        {
                            subClassIndex = 0;
                            Models.Domain temp = new Domain();
                            temp.Title = linesMatrix[i][4];
                            temp.ID = linesMatrix[i][0];
                            temp.ListOfServices = new List<Service>();
                            ListOfMainClass.Add(temp);
                            ++mainClassIndex;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(linesMatrix[i][1]))// This is Service
                            {
                                subSubClassIndex = 0;
                                Models.Service temp = new Service();
                                temp.Title = linesMatrix[i][4];
                                temp.ID = linesMatrix[i][1];
                                temp.ListOfSubServices = new List<SubService>();
                                ListOfMainClass[mainClassIndex - 1].ListOfServices.Add(temp);
                                ++subClassIndex;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(linesMatrix[i][2]))// this is SubServices
                                {
                                    questionIndex = 0;
                                    Models.SubService temp = new SubService();
                                    temp.Title = linesMatrix[i][4];
                                    temp.ID = linesMatrix[i][2];
                                    temp.ListOfQuestions = new List<Questions>();
                                    ListOfMainClass[mainClassIndex - 1].ListOfServices[subClassIndex - 1].ListOfSubServices.Add(temp);
                                    ListOfSubServices.Add(temp);
                                    ++subSubClassIndex;
                                }
                                else // this is question
                                {
                                    Models.Questions temp = new Questions();

                                    temp.ID = linesMatrix[i][3];
                                    temp.Question = linesMatrix[i][4];
                                    temp.Header = temp.Question;
                                    temp.QuestionAnswer = Models.Answer.Nie;
                                    temp.Wage = linesMatrix[i][5];
                                    temp.PMax = linesMatrix[i][6];
                                    temp.PMin = linesMatrix[i][7];

                                    ListOfMainClass[mainClassIndex - 1].ListOfServices[subClassIndex - 1].ListOfSubServices[subSubClassIndex - 1].ListOfQuestions.Add(temp);
                                    ListOfQuestions.Add(temp);
                                    ++questionIndex;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void InitializeTreeViewForAudit()
        {
            TreeViewItem treeItem_1 = null;
            TreeViewItem treeItem_2 = null;
            TreeViewItem treeItem_3 = null;

            for (int i = 0; i < ListOfMainClass.Count; ++i)
            {
                treeItem_1 = new TreeViewItem() { Header = ListOfMainClass[i].Title };
                for (int j = 0; j < ListOfMainClass[i].ListOfServices.Count; ++j)
                {
                    treeItem_2 = new TreeViewItem() { Header = ListOfMainClass[i].ListOfServices[j].Title };
                    for (int k = 0; k < ListOfMainClass[i].ListOfServices[j].ListOfSubServices.Count; ++k)
                    {
                        treeItem_3 = new TreeViewItem() { Header = ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].Title };
                        for (int x = 0; x < ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].ListOfQuestions.Count; ++x)
                        {
                            treeItem_3.Items.Add(ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].ListOfQuestions[x]);
                        }
                        treeItem_2.Items.Add(treeItem_3);
                    }
                    treeItem_1.Items.Add(treeItem_2);
                }
                GUI_Audit_QuestionsTreeView.Items.Add(treeItem_1);

            }
        }

        private void InitializeGUI_Audit_DomainsTree()
        {
            TreeViewItem treeItem_1 = null;
            TreeViewItem treeItem_2 = null;

            for (int i = 0; i < ListOfMainClass.Count; ++i)
            {
                treeItem_1 = new TreeViewItem() { Header = ListOfMainClass[i].Title };
                for (int j = 0; j < ListOfMainClass[i].ListOfServices.Count; ++j)
                {
                    treeItem_2 = new TreeViewItem() { Header = ListOfMainClass[i].ListOfServices[j].Title };
                    treeItem_1.Items.Add(treeItem_2);
                }
                GUI_Audit_DomainsTreeView.Items.Add(treeItem_1);
            }


        }
        private void GUI_Audit_QuestionsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if ((sender as TreeView).SelectedItem is Models.Questions)
            {
                CurrentQuestion = ((sender as TreeView).SelectedItem as Models.Questions); // showing values from node
                GUI_Audit_QuestionBox.Text = CurrentQuestion.Question;
                GUI_Audit_AnswersBox.Text = CurrentQuestion.Comment;
                GUI_Audit_SupportBox.Text = CurrentQuestion.Support;

                TreeViewItem parent = ((sender as TreeView).SelectedItem as TreeViewItem).Parent as TreeViewItem; // prepare for editining GUI_Audit_Subservice label
                if (parent != null)
                {
                    GUI_Audit_Subservice.Text = CurrentQuestion.ID + " - " + parent.Header;
                }

                int indexInList = 0;
                foreach (Models.Questions item in ListOfQuestions) // search for node in List for change value of slider
                {
                    if (item == CurrentQuestion)
                    {
                        GUI_Audit_Slider.Value = indexInList;
                    }
                    ++indexInList;
                }

                // Radio buttons:
                switch (CurrentQuestion.QuestionAnswer)
                {
                    case Models.Answer.Bezprzedmiotowo:
                        {
                            GUI_Audit_Radio_WTF.IsChecked = true;
                            break;
                        }
                    case Models.Answer.BrakOdp:
                        {
                            GUI_Audit_Radio_NoAnser.IsChecked = true;
                            break;
                        }
                    case Models.Answer.Nie:
                        {
                            GUI_Audit_Radio_No.IsChecked = true;
                            break;
                        }
                    case Models.Answer.Tak:
                        {
                            GUI_Audit_Radio_Yes.IsChecked = true;
                            break;
                        }
                }
            }
        }

        private void GUI_Audit_RadioButton_Checked(object sender, RoutedEventArgs e) // Change option for node due user decision
        {
            if (CurrentQuestion != null)
            {
                if (GUI_Audit_Radio_Yes.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.Tak;
                }
                if (GUI_Audit_Radio_No.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.Nie;
                }
                if (GUI_Audit_Radio_NoAnser.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.BrakOdp;
                }
                if (GUI_Audit_Radio_WTF.IsChecked == true)
                {
                    CurrentQuestion.QuestionAnswer = Models.Answer.Bezprzedmiotowo;
                }

                //Sprawdzenie RadioBoxa w zakładce zagrożenia
                if (GUI_Danger_Radio_Likely.IsChecked == true)
                {
                    setDangerDangerValue(GUI_Danger_ID.Text, "3");
                }
                else if (GUI_Danger_Radio_Unlikely.IsChecked == true)
                {
                    setDangerDangerValue(GUI_Danger_ID.Text, "2");
                }
                else if (GUI_Danger_Radio_VeryLikely.IsChecked == true)
                {
                    setDangerDangerValue(GUI_Danger_ID.Text, "4");
                }
                else if (GUI_Danger_Radio_VeryUnlikely.IsChecked == true)
                {
                    setDangerDangerValue(GUI_Danger_ID.Text, "1");
                }
            }
        }

        private void GUI_Audit_AnswersBox_TextChanged(object sender, TextChangedEventArgs e) // Change comment for node due user change
        {
            if (CurrentQuestion != null)
            {
                CurrentQuestion.Comment = GUI_Audit_AnswersBox.Text;
            }
        }

        private void GUI_Audit_GoLeft_Click(object sender, RoutedEventArgs e) // Go left in slider
        {
            if (CurrentQuestion != null)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView((GUI_Audit_Slider.Value - 1) >= 0 ? (int)GUI_Audit_Slider.Value - 1 : ListOfQuestions.Count - 1);
            }
        }

        private void GUI_Audit_GoRight_Click(object sender, RoutedEventArgs e) // Go right in slider
        {
            if (CurrentQuestion != null)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView(((int)GUI_Audit_Slider.Value + 1) % ListOfQuestions.Count);
            }
        }

        private void GUI_Audit_Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) // Event for slider value change
        {
            if (CurrentQuestion != null)
            {
                ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView((int)GUI_Audit_Slider.Value);
            }
        }

        private void ChangeSelectedTreeViewItemIn_GUI_Audit_QuestionsTreeView(int number) // hiding and expanding specified node in TreeView
        {
            TreeViewItem temp = null;
            foreach (TreeViewItem Lay1 in GUI_Audit_QuestionsTreeView.Items)
            {
                Lay1.IsExpanded = false;
                foreach (TreeViewItem Lay2 in Lay1.Items)
                {
                    Lay2.IsExpanded = false;
                    foreach (TreeViewItem Lay3 in Lay2.Items)
                    {
                        Lay3.IsExpanded = false;
                        foreach (Models.Questions Lay4 in Lay3.Items)
                        {
                            Lay4.IsExpanded = false;
                            if (Lay4 == ListOfQuestions[number])
                            {
                                Lay4.IsExpanded = true;
                                Lay4.IsSelected = true;
                                temp = Lay4 as TreeViewItem;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < 3; ++i)
            {
                temp = (temp.Parent as TreeViewItem);
                temp.IsExpanded = true;
            }
        }

        private void GUI_Audit_DomainsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem temp = null;
            foreach (TreeViewItem domain in GUI_Audit_DomainsTreeView.Items)
            {
                domain.IsExpanded = false;
                foreach (TreeViewItem item in domain.Items)
                {
                    item.IsExpanded = false;
                    if ((sender as TreeView).SelectedItem == item)
                    {
                        temp = item;
                    }


                }
            }
            if (temp != null) // found selected TreeViewItem
            {
                temp.IsExpanded = true;
                (temp.Parent as TreeViewItem).IsExpanded = true;


            }

        }

        #endregion Audit_Tab

        #region Special_Actions

        private void GUI_SpecialActions_MinimalValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            GUI_SpecialActions_SliderValue.Content = GUI_SpecialActions_MinimalValueSlider.Value.ToString("F1");

        }

        void SpecialActionsInit()
        {
            GUI_SpecialActions_ScenarioTree.Items.Clear();

            List<Scenario2> subScenario = new List<Scenario2>();
            foreach (Scenario2 sa in Scenario2List)
            {
                subScenario.Add(sa);
            }

            List<string> scenarioList = new List<string>();
            foreach (Scenario2 sa in subScenario)
            {
                if (!scenarioList.Contains(sa.Scenario))
                {
                    scenarioList.Add(sa.Scenario);
                }
            }

            foreach (string str in scenarioList)
            {
                TreeViewItem item = new TreeViewItem() { Header = str };
                List<TreeViewItem> items = FindAllForScenario(subScenario, str);
                foreach (TreeViewItem tvi in items)
                {
                    item.Items.Add(tvi);
                }
                GUI_SpecialActions_ScenarioTree.Items.Add(item);
            }
        }

        private void GUI_SpecialActions_ScenarioTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            foreach (TreeViewItem item in GUI_SpecialActions_ScenarioTree.Items)
            {
                foreach (TreeViewItem item2 in item.Items)
                {
                    foreach (TreeViewItem item3 in item2.Items)
                    {
                        if (item3 == (sender as TreeView).SelectedItem)
                        {
                            _Values_ServiceImprovement = new ObservableCollection<Values_ServiceImprovement>();
                            //GUI_Scenario_IncludedCellsInScenarioTree.Items.Clear();
                            foreach (Scenario scen in algorithm.GetScenarioData())
                            {
                                if (scen.Description == (string)item3.Header)
                                {
                                    _Values_ServiceImprovement.Add(new Values_ServiceImprovement()
                                    {
                                        CurrentIndex = 0.0,
                                        ServiceID = scen.Probability.ToString(),
                                        Name = scen.Risk.ToString(),
                                        AuditSection = scen.Consequence.ToString()
                                    });

                                    //GUI_Scenario_IncludedCellsInScenarioTree.Items.Add(new TreeViewItem() { Header = scen.ID });

                                    GUI_SpecialActions_Scenario_Value.Content = scen.ID;
                                    GUI_SpecialActions_Status_Value.Content = "";
                                    GUI_SpecialActions_DISS_Value.Content = scen.DisruptiveActions;
                                    GUI_SpecialActions_Expo_Value.Content = scen.Probability;
                                    GUI_SpecialActions_PREV_Value.Content = scen.PreventiveActions;
                                    GUI_SpecialActions_PALL_Value.Content = scen.SoothingActions;
                                    GUI_SpecialActions_PROT_Value.Content = scen.ProtectiveActions;
                                    GUI_SpecialActions_RECUP_Value.Content = scen.ReproductiveActions;






                                }
                            }
                            GUI_SpecialActions_ServiceImprovementGrid.ItemsSource = _Values_ServiceImprovement;
                            GUI_SpecialActions_ServiceImprovementGrid.Items.Refresh();
                            PropertyChanged(this, new PropertyChangedEventArgs("GUI_SpecialActions_ServiceImprovementGrid"));
                            NotifyPropertyChanged("_Values_ServiceImprovement");
                        }
                    }
                }
            }
        }

        #endregion Special_Actions

        #region Values

        void InitializeTreeViewForValues()
        {
            TreeViewItem treeItem = null;
            TreeViewItem serviceTreeItem = null;


            for (int i = 0; i < ListOfMainClass.Count; ++i)
            {
                treeItem = new TreeViewItem();
                treeItem.Header = ListOfMainClass[i].Title;
                treeItem.Uid = i.ToString();
                //treeItem.Selected += new RoutedEventHandler((sender, e) => GUI_Charts_DomainSelected(sender, e, _ListOfMainClass));

                for (int j = 0; j < ListOfMainClass[i].ListOfServices.Count; j++)
                {
                    serviceTreeItem = new TreeViewItem();
                    serviceTreeItem.Header = ListOfMainClass[i].ListOfServices[j].Title;

                    var i1 = i;
                    var j1 = j;
                    serviceTreeItem.Selected += new RoutedEventHandler((sender, e) => GUI_Values_ServiceSelected(sender, e, ListOfMainClass[i1].ListOfServices[j1]));
                    treeItem.Items.Add(serviceTreeItem);
                }
                GUI_Values_AuditChartTree.Items.Add(treeItem);
            }
        }

        private void GUI_Values_ServiceSelected(object sender, RoutedEventArgs e, Service service)
        {
            DataContext = this;


            List<string> names = new List<string>();
            List<double> values = new List<double>();
            List<double> oldValues = new List<double>();
            _Values_GridView = new ObservableCollection<Values_GridView>();

            foreach (var scenario in service.ListOfSubServices)
            {
                var oldScenario = OldListOfSubServices.Find(x => x.ID == scenario.ID);
                names.Add(scenario.ID); values.Add(Math.Round(scenario.averageValue, 2)); oldValues.Add(Math.Round(oldScenario.averageValue, 2));
                _Values_GridView.Add(new Values_GridView()
                {
                    ID = scenario.ID,
                    Title = scenario.Title,
                    Value_K = Math.Round(scenario.averageValue, 2),
                    Value_P = Math.Round(oldScenario.averageValue, 2)
                });
            }

            NotifyPropertyChanged("_Values_GridView"); // NotifyPropertyChanged(nameof(_Values_GridView));

            Values_Chart_Axes = names.ToArray();

            Values_Chart_Lines = new ObservableCollection<ChartLine>
            {
                new ChartLine
                {
                    LineColor = Colors.Red,
                    FillColor = Color.FromArgb(128, 255, 0, 0),
                    LineThickness = 2,
                    PointDataSource = values,
                    Name = "Wartosc K"
                },
                new ChartLine
                {
                    LineColor = Colors.Blue,
                    FillColor = Color.FromArgb(128, 0, 0, 255),
                    LineThickness = 2,
                    PointDataSource = oldValues,
                    Name = "Wartosc P"
                }
            };

            PropertyChanged(this, new PropertyChangedEventArgs("Values_Chart_Lines"));
            PropertyChanged(this, new PropertyChangedEventArgs("Values_Chart_Axes"));
            PropertyChanged(this, new PropertyChangedEventArgs("GUI_Values_Chart"));

        }


        #endregion Values

        #region ResourcesTab 

        void InitializeTreeViewForResources(List<Models.ResourcesType> _ListOfResources) //initializing tree of resources 
        {
            TreeViewItem treeItem = null;
            TreeViewItem treeItem_2 = null;
            for (int i = 0; i < _ListOfResources.Count; i++)
            {
                treeItem = new TreeViewItem();
                treeItem.Header = _ListOfResources[i].Title;

                for (int j = 0; j < _ListOfResources[i].ListOfResources.Count; j++)
                {
                    treeItem_2 = new TreeViewItem();
                    treeItem_2.Header = _ListOfResources[i].ListOfResources[j].Description + " - " + _ListOfResources[i].ListOfResources[j].ResourcesValue;
                    treeItem_2.Uid = j.ToString();
                    treeItem_2.Selected += new RoutedEventHandler(ResourceChosen);
                    treeItem.Items.Add(treeItem_2);
                }

                GUI_Resources_TreeView.Items.Add(treeItem);
            }
        }


        private void ResourceChosen(object sender, RoutedEventArgs e) //change textbox with name when user chooses resource 
        {
            foreach (var resoList in ResourcesList)
            {
                foreach (var reso in resoList.ListOfResources)
                {

                    if ((sender as TreeViewItem).Header.ToString().StartsWith(reso.Description))
                    {
                        CurrentResource = reso;

                    }
                }
            }
            GUI_Resources_TextBox.Clear();

            GUI_Resources_Availability_TextBox.Text = CurrentResource.AvailabilityConsequence;
            GUI_Resources_Integrity_TextBox.Text = CurrentResource.IntegrityConsequence;
            GUI_Resources_Privacy_TextBox.Text = CurrentResource.PrivacyConsequence;
            GUI_Resources_Comment_TextBox.Text = CurrentResource.Comment;


            // GUI_Resources_Availability_TextBox.TextChanged += new TextChangedEventHandler((sender2, e2) => AvailabilityTextBoxChanged(sender2, e2, CurrentResource)); 

            TreeViewItem item = (TreeViewItem)GUI_Resources_TreeView.SelectedItem;

            GUI_Resources_TextBox.Text = item.Header.ToString();




        }


        private void AvailabilityScenarioTree(object sender, MouseButtonEventArgs e) //if user click Availability show Availability scenarios 
        {
            GUI_ResourcesAvailability_TreeView.Items.Clear();

            TreeViewItem treeItem = null;

            for (int i = 0; i < ScenarioList.Count; i++)
            {
                if (ScenarioList[i].ConsequenceCriteria.Equals("Dostępność"))
                {
                    treeItem = new TreeViewItem();
                    treeItem.Header = ScenarioList[i].Description;

                    GUI_ResourcesAvailability_TreeView.Items.Add(treeItem);
                }
            }
        }

        private void IntegrityScenarioTree(object sender, MouseButtonEventArgs e) //if user click Integrity show Integrity scenarios 
        {

            GUI_ResourcesAvailability_TreeView.Items.Clear();

            TreeViewItem treeItem = null;

            for (int i = 0; i < ScenarioList.Count; i++)
            {
                if (ScenarioList[i].ConsequenceCriteria.Equals("Integralność"))
                {
                    treeItem = new TreeViewItem();
                    treeItem.Header = ScenarioList[i].Description;

                    GUI_ResourcesAvailability_TreeView.Items.Add(treeItem);
                }
            }
        }

        private void PrivacyScenarioTree(object sender, MouseButtonEventArgs e) //if user click Privacy show Privacy scenarios 
        {

            GUI_ResourcesAvailability_TreeView.Items.Clear();

            TreeViewItem treeItem = null;

            for (int i = 0; i < ScenarioList.Count; i++)
            {
                if (ScenarioList[i].ConsequenceCriteria.Equals("Poufność"))
                {
                    treeItem = new TreeViewItem();
                    treeItem.Header = ScenarioList[i].Description;

                    GUI_ResourcesAvailability_TreeView.Items.Add(treeItem);
                }
            }
        }


        private void AvailabilityTextBoxChanged(object sender, TextChangedEventArgs e)
        {
            foreach (var item in ResourcesList)
            {
                foreach (var item2 in item.ListOfResources)
                {
                    item2.AvailabilityConsequence = PIA_Input_Validation(GUI_Resources_Availability_TextBox);
                }
            }

        }

        private void IntegrityTextBoxChanged(object sender, TextChangedEventArgs e)
        {
            foreach (var item in ResourcesList)
            {
                foreach (var item2 in item.ListOfResources)
                {
                    item2.IntegrityConsequence = PIA_Input_Validation(GUI_Resources_Integrity_TextBox);
                }
            }
        }

        private void PrivacyTextBoxChanged(object sender, TextChangedEventArgs e)
        {
            foreach (var item in ResourcesList)
            {
                foreach (var item2 in item.ListOfResources)
                {
                    item2.PrivacyConsequence = PIA_Input_Validation(GUI_Resources_Privacy_TextBox);
                }
            }
        }

        private void GUI_Resources_Comment_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (CurrentResource != null)
            {
                CurrentResource.Comment = GUI_Resources_Comment_TextBox.Text;
            }
        }

        private string PIA_Input_Validation(TextBox textBox)
        {
            int value;
            bool result = Int32.TryParse(textBox.Text, out value);
            if (result)
            {
                if (value > 4)
                {
                    textBox.Text = "4";
                    textBox.CaretIndex = textBox.Text.Length;
                    return "4";
                }

                else if (value < 1)
                {
                    textBox.Text = "1";
                    textBox.CaretIndex = textBox.Text.Length;
                    return "1";
                }

                else
                    return textBox.Text;
            }

            else
                textBox.Text = "";
            return "1";
        }
        #endregion

        #region RiskCalc

        //private void InitializeResorces()
        //{
        //    ResourcesDict = new Dictionary<string, string>();
        //    StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/ZASOBY-ex.csv", Encoding.UTF8);  //hardcoding FTW
        //    string input = SR.ReadToEnd();
        //    string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { ';' }, StringSplitOptions.None)).ToArray();
        //    for (int i = 3; i < linesMatrix.GetLength(0); ++i)
        //    {
        //        if (!String.IsNullOrEmpty(linesMatrix[i][2])) // this is resource
        //        {
        //            ResourcesDict.Add(linesMatrix[i][2], linesMatrix[i][4]);
        //        }
        //    }
        //}

        /////// wkleilem
        void InitializeResorces()
        {
            ResourcesDict = new Dictionary<string, string>();
            ResourcesList = new List<Models.ResourcesType>();
            StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/ZASOBY-ex.csv", Encoding.UTF8);  //hardcoding FTW 
            string input = SR.ReadToEnd();
            string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { ';' }, StringSplitOptions.None)).ToArray();
            int resourceNumber = 0;

            for (int i = 2; i < linesMatrix.GetLength(0); ++i)
            {
                if (String.IsNullOrEmpty(linesMatrix[i][0]))
                {
                    if (String.IsNullOrEmpty(linesMatrix[i][2])) //this is type of resource 
                    {
                        Models.ResourcesType resourceType = new Models.ResourcesType()
                        {
                            Title = linesMatrix[i][1],
                            ListOfResources = new List<Models.Resources>()
                        };
                        ResourcesList.Add(resourceType);
                        ++resourceNumber;
                    }
                    else // this is resource 
                    {
                        ResourcesDict.Add(linesMatrix[i][2], linesMatrix[i][4]);

                        Models.Resources resource = new Models.Resources()
                        {
                            ID = linesMatrix[i][1],
                            Description = linesMatrix[i][2],
                            ResourcesValue = linesMatrix[i][3],

                            IntegrityConsequence = "1",
                            PrivacyConsequence = "1",
                            AvailabilityConsequence = "1"

                        };

                        ResourcesList[resourceNumber - 1].ListOfResources.Add(resource);
                    }
                }
            }
        }
        /// wkleilem

        private void InitializeDanger()
        {
            DangerDict = new Dictionary<string, string>();
            DangerList = new List<Danger>();
            StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/ZAGROZ-ex.csv", Encoding.UTF8);  //hardcoding FTW
            string input = SR.ReadToEnd();
            string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { '\t' }, StringSplitOptions.None)).ToArray();
            for (int i = 2; i < linesMatrix.GetLength(0); ++i)
            {
                if (!String.IsNullOrEmpty(linesMatrix[i][1])) // this is danger
                {
                    DangerDict.Add(linesMatrix[i][1], linesMatrix[i][3]);
                    Danger danger = new Danger()
                    {
                        Incident = linesMatrix[i][1],
                        Description = linesMatrix[i][2],
                        DangerValue = linesMatrix[i][3]
                    };
                    DangerList.Add(danger);
                }
            }
        }

        private void InitializeScenarioList()
        {
            ScenarioList = new List<Scenario>();
            StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/SCEN_DANEANAL-ex.csv", Encoding.UTF8);  //hardcoding FTW
            string input = SR.ReadToEnd();
            string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { '\t' }, StringSplitOptions.None)).ToArray();

            for (int i = 3; i < linesMatrix.GetLength(0); ++i)
            {
                if (!String.IsNullOrEmpty(linesMatrix[i][2])) // this is scenario
                {
                    Models.Scenario newScenario = new Models.Scenario();

                    newScenario.ID = linesMatrix[i][2];
                    newScenario.Description = linesMatrix[i][3];
                    newScenario.DisruptiveActions = linesMatrix[i][4];
                    newScenario.PreventiveActions = linesMatrix[i][5];
                    newScenario.ProtectiveActions = linesMatrix[i][6];
                    newScenario.SoothingActions = linesMatrix[i][7];
                    newScenario.ReproductiveActions = linesMatrix[i][8];
                    newScenario.ConsequenceCriteria = linesMatrix[i][9];
                    newScenario.CauseCriteria = linesMatrix[i][10];
                    if (DangerDict.ContainsKey(linesMatrix[i][11]))
                    {
                        newScenario.Danger = DangerDict[linesMatrix[i][11]];
                    }
                    else
                    {
                        newScenario.Danger = String.Empty;
                    }
                    if (ResourcesDict.ContainsKey(linesMatrix[i][12]))
                    {
                        newScenario.Resource = ResourcesDict[linesMatrix[i][12]];
                    }
                    else
                    {
                        newScenario.Resource = String.Empty;
                    }
                    ScenarioList.Add(newScenario);
                }
                else
                {
                    continue;
                }
            }
        }
        private void InitializeScenario2List()
        {
            Scenario2List = new List<Scenario2>();
            StreamReader SR = new StreamReader(Environment.CurrentDirectory + "/CSV/SCEN_DANEANAL-ex2.csv", Encoding.UTF8);  //hardcoding FTW
            string input = SR.ReadToEnd();
            string[][] linesMatrix = input.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Split(new[] { ';' }, StringSplitOptions.None)).ToArray();

            for (int i = 1; i < linesMatrix.GetLength(0); ++i)
            {
                Models.Scenario2 newScenario = new Models.Scenario2();
                newScenario.Scenario = linesMatrix[i][0];
                newScenario.Cause = linesMatrix[i][1];
                newScenario.Description = linesMatrix[i][2];
                newScenario.CauseCriteria = linesMatrix[i][3];
                newScenario.Danger = linesMatrix[i][4];
                newScenario.Resource = linesMatrix[i][5];
                Scenario2List.Add(newScenario);
            }
        }

        private void GUI_NaturalDanger_Tree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem selectedItem = (TreeViewItem)GUI_NaturalDanger_Tree.SelectedItem;
            string header = (string)selectedItem.Header;
            if (!header.Contains("-"))
            {
                return;
            }

            string incident = header.Split('-')[0];
            GUI_Danger_ID.Text = incident;

            string description = header.Split('-')[1];
            GUI_Danger_Description.Text = description;

            string dangerValue = "";
            foreach (Danger danger in DangerList)
            {
                if (danger.Incident.Equals(incident))
                {
                    dangerValue = danger.DangerValue;
                }
            }

            if (dangerValue.Equals("1"))
            {
                GUI_Danger_Radio_VeryUnlikely.IsChecked = true;
            }
            else if (dangerValue.Equals("2"))
            {
                GUI_Danger_Radio_Unlikely.IsChecked = true;
            }
            else if (dangerValue.Equals("3"))
            {
                GUI_Danger_Radio_Likely.IsChecked = true;
            }
            else if (dangerValue.Equals("4"))
            {
                GUI_Danger_Radio_VeryLikely.IsChecked = true;
            }

            InitGUI_DangerScenario_Tree(incident);
        }

        void InitializeGUI_NaturalDanger_Tree()
        {
            //TODO dodać do wczytywania CSV opis grupujący
            TreeViewItem accidentsTreeViewItem = new TreeViewItem() { Header = "Wypadki / Zdarzenia losowe" };
            TreeViewItem errorsTreeViewItem = new TreeViewItem() { Header = "Błędy" };
            TreeViewItem activitiesTreeViewItem = new TreeViewItem() { Header = "Działania Celowe" };

            for (int i = 0; i < DangerList.Count; i++)
            {
                if (DangerList[i].Incident.Substring(0, 2).Equals("AC"))
                {
                    accidentsTreeViewItem.Items.Add(new TreeViewItem() { Header = DangerList[i].Incident + "-" + DangerList[i].Description });
                }
                else if (DangerList[i].Incident.Substring(0, 2).Equals("ER"))
                {
                    errorsTreeViewItem.Items.Add(new TreeViewItem() { Header = DangerList[i].Incident + "-" + DangerList[i].Description });
                }
                else if (DangerList[i].Incident.Substring(0, 2).Equals("MA"))
                {
                    activitiesTreeViewItem.Items.Add(new TreeViewItem() { Header = DangerList[i].Incident + "-" + DangerList[i].Description });
                }
            }

            GUI_NaturalDanger_Tree.Items.Add(accidentsTreeViewItem);
            GUI_NaturalDanger_Tree.Items.Add(errorsTreeViewItem);
            GUI_NaturalDanger_Tree.Items.Add(activitiesTreeViewItem);
        }

        void InitGUI_DangerScenario_Tree(string danger)
        {
            GUI_DangerScenario_Tree.Items.Clear();

            List<Scenario2> subScenario = new List<Scenario2>();
            foreach (Scenario2 sa in Scenario2List)
            {
                if (sa.Danger.Equals(danger))
                {
                    subScenario.Add(sa);
                }
            }

            List<string> scenarioList = new List<string>();
            foreach (Scenario2 sa in subScenario)
            {
                if (!scenarioList.Contains(sa.Scenario))
                {
                    scenarioList.Add(sa.Scenario);
                }
            }

            foreach (string str in scenarioList)
            {
                TreeViewItem item = new TreeViewItem() { Header = str };
                List<TreeViewItem> items = FindAllForScenario(subScenario, str);
                foreach (TreeViewItem tvi in items)
                {
                    item.Items.Add(tvi);
                }
                GUI_DangerScenario_Tree.Items.Add(item);
            }
        }

        void InitializeGUI_RiskAssessment_ScenarioTree()
        {
            GUI_RiskAssessment_ScenarioTree.Items.Clear();

            List<Scenario2> subScenario = new List<Scenario2>();
            foreach (Scenario2 sa in Scenario2List)
            {
                subScenario.Add(sa);
            }

            List<string> scenarioList = new List<string>();
            foreach (Scenario2 sa in subScenario)
            {
                if (!scenarioList.Contains(sa.Scenario))
                {
                    scenarioList.Add(sa.Scenario);
                }
            }

            foreach (string str in scenarioList)
            {
                TreeViewItem item = new TreeViewItem() { Header = str };
                List<TreeViewItem> items = FindAllForScenario(subScenario, str);
                foreach (TreeViewItem tvi in items)
                {
                    item.Items.Add(tvi);
                }
                GUI_RiskAssessment_ScenarioTree.Items.Add(item);
            }
        }

        void InitializeGUI_RiskAssessment_WeightGrid()
        {
            DataContext = this;
            _Values_RiskAssessment.Clear();

            for (int i = 0; i < ScenarioList.Count; i++)
            {
                _Values_RiskAssessment.Add(new Values_RiskAssessment() { Group = ScenarioList[i].ID, ScenarioGroupName = ScenarioList[i].Description, InitialWeight = OldScenarioList[i].Risk, FinalWieght = ScenarioList[i].Risk });
            }
        }

        void Refresh_Risks_Chart()
        {
            DataContext = this;
            List<string> names = new List<string>();
            List<double> values = new List<double>();
            List<double> oldValues = new List<double>();

            for (int i = 0; i < ScenarioList.Count; i++)
            {
                names.Add(ScenarioList[i].ID);
                values.Add(ScenarioList[i].Risk);
                oldValues.Add(OldScenarioList[i].Risk);
            }

            Risk_Chart_Axes = names.ToArray();

            Risk_Chart_Lines = new ObservableCollection<ChartLine>
            {
                new ChartLine
                {
                    LineColor = Colors.Red,
                    FillColor = Color.FromArgb(128, 255, 0, 0),
                    LineThickness = 2,
                    PointDataSource = values,
                    Name = "Ryzyko"
                },
                new ChartLine
                {
                    LineColor = Colors.Blue,
                    FillColor = Color.FromArgb(128, 0, 0, 255),
                    LineThickness = 2,
                    PointDataSource = oldValues,
                    Name = "Poprzednie ryzyko"
                }
            };

            NotifyPropertyChanged("Risk_Chart_Axes");
            NotifyPropertyChanged("Risk_Chart_Lines");
        }

        List<TreeViewItem> FindAllForCause(List<Scenario2> list, string cause)
        {
            List<TreeViewItem> retVal = new List<TreeViewItem>();
            foreach (Scenario2 scen in list)
            {
                if (scen.Cause.Equals(cause))
                {
                    retVal.Add(new TreeViewItem() { Header = scen.Description });
                }
            }

            return retVal;
        }

        List<TreeViewItem> FindAllForScenario(List<Scenario2> list, string scenario)
        {
            List<TreeViewItem> retVal = new List<TreeViewItem>();

            List<string> causeList = new List<string>();
            foreach (Scenario2 scen in list)
            {
                if (!causeList.Contains(scen.Cause) && scen.Scenario.Equals(scenario))
                {
                    causeList.Add(scen.Cause);
                }
            }

            foreach (string str in causeList)
            {
                TreeViewItem item = new TreeViewItem() { Header = str };
                List<TreeViewItem> items = FindAllForCause(list, str);
                foreach (TreeViewItem tvi in items)
                {
                    item.Items.Add(tvi);
                }
                retVal.Add(item);
            }

            return retVal;
        }
        void setDangerDangerValue(string danagerNumber, string value)
        {
            DangerList.Find(d => d.Incident == danagerNumber).DangerValue = value;
        }
        #endregion RiskCalc
        #region ChartTab

        void InitializeTreeViewForCharts(List<Models.Domain> _ListOfMainClass)
        {
            TreeViewItem treeItem = null;
            for (int i = 0; i < _ListOfMainClass.Count; ++i)
            {
                treeItem = new TreeViewItem();
                treeItem.Header = _ListOfMainClass[i].Title;
                treeItem.Uid = i.ToString();
                treeItem.Selected += new RoutedEventHandler((sender, e) => GUI_Charts_DomainSelected(sender, e, _ListOfMainClass));

                //    myTimer.Elapsed += new ElapsedEventHandler((sender, e) => PlayMusicEvent(sender, e, musicNote));



                GUI_Charts_AuditDomainTreeView.Items.Add(treeItem);
            }
        }

        private void GUI_Charts_DomainSelected(object sender, RoutedEventArgs e, List<Models.Domain> _ListOfMainClass)
        {
            GUI_Charts_AuditTreeView.Items.Clear();
            TreeViewItem item = (TreeViewItem)GUI_Charts_AuditDomainTreeView.SelectedItem;

            TreeViewItem treeItem_1 = null;
            TreeViewItem treeItem_2 = null;
            //  TreeViewItem treeItem_1 = null;

            int i = Int32.Parse(item.Uid);

            for (int j = 0; j < _ListOfMainClass[i].ListOfServices.Count; ++j)
            {
                treeItem_1 = new TreeViewItem() { Header = _ListOfMainClass[i].ListOfServices[j].Title };

                for (int k = 0; k < _ListOfMainClass[i].ListOfServices[j].ListOfSubServices.Count; ++k)
                {
                    treeItem_2 = new TreeViewItem() { Header = _ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].Title };
                    treeItem_2.Selected += new RoutedEventHandler((sender2, e2) => GUI_Charts_SelectedSubService(sender2, e2));
                    //for (int x = 0; x < _ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].ListOfQuestions.Count; ++x)
                    //{
                    //    treeItem_3.Items.Add(_ListOfMainClass[i].ListOfServices[j].ListOfSubServices[k].ListOfQuestions[x]);
                    //}
                    //treeItem_2.Items.Add(treeItem_2);

                    treeItem_1.Items.Add(treeItem_2);
                }


                GUI_Charts_AuditTreeView.Items.Add(treeItem_1);

            }

        }
        private void GUI_Charts_SelectedSubService(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)GUI_Charts_AuditTreeView.SelectedItem;

            var SubService = ListOfSubServices.Find(x => x.Title == item.Header.ToString());
            var OldSubService = OldListOfSubServices.Find(x => x.Title == item.Header.ToString());

            DataContext = this;

            List<string> names = new List<string>();
            List<double> values = new List<double>();
            List<double> oldValues = new List<double>();

            foreach (Questions temp in SubService.ListOfQuestions)
            {
                int value = 0;
                var question = ListOfQuestions.Find(x => x.ID == temp.ID);
                if (question.QuestionAnswer == Answer.Tak)
                    value = Int32.Parse(question.Wage);

                values.Add(value);

                names.Add(temp.ID);

            }

            foreach (Questions temp in OldSubService.ListOfQuestions)
            {
                int value = 0;
                var question = OldSubService.ListOfQuestions.Find(x => x.ID == temp.ID);
                if (question.QuestionAnswer == Answer.Tak)
                    value = Int32.Parse(question.Wage);

                oldValues.Add(value);
            }
            Charts_Chart_Axes = names.ToArray();

            Charts_Chart_Lines = new ObservableCollection<ChartLine>
            {
                new ChartLine
                {
                    LineColor = Colors.Red,
                    FillColor = Color.FromArgb(128, 255, 0, 0),
                    LineThickness = 2,
                    PointDataSource = values,
                    Name = "Wartość K"
                },
                new ChartLine
                {
                    LineColor = Colors.Blue,
                    FillColor = Color.FromArgb(128, 0, 0, 255),
                    LineThickness = 2,
                    PointDataSource = oldValues,
                    Name = "Wartość P"
                }
            };

            PropertyChanged(this, new PropertyChangedEventArgs("Charts_Chart_Lines"));
            PropertyChanged(this, new PropertyChangedEventArgs("Charts_Chart_Axes"));
            PropertyChanged(this, new PropertyChangedEventArgs("GUI_Charts_Chart"));
        }
        #endregion
        private readonly Random random = new Random(666);

        private void ScrollBar_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }

        public List<double> GenerateRandomDataSet(int nmbrOfPoints)
        {
            var pts = new List<double>(nmbrOfPoints);
            for (var i = 0; i < nmbrOfPoints; i++)
            {
                pts.Add(random.NextDouble());
            }
            return pts;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // DataContext = this;

            // Charts_Chart_Axes = new[] { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7" };

            //Charts_Chart_Lines = new ObservableCollection<ChartLine>
            //{
            //    new ChartLine
            //    {
            //        LineColor = Colors.Red,
            //        FillColor = Color.FromArgb(128, 255, 0, 0),
            //        LineThickness = 2,
            //        PointDataSource = GenerateRandomDataSet(Values_Axes.Length),
            //        Name = "Chart 1"
            //    },
            //    new ChartLine
            //    {
            //        LineColor = Colors.Blue,
            //        FillColor = Color.FromArgb(128, 0, 0, 255),
            //        LineThickness = 2,
            //        PointDataSource = GenerateRandomDataSet(Values_Axes.Length),
            //        Name = "Chart 2"
            //    }
            //};

            //_Values_GridView = new ObservableCollection<Values_GridView>() {
            //    new Values_GridView()
            //    {
            //       Cell = "1", Field = "2", Title = "3333", Value_K = 2.3, Value_P = 1.3
            //    },
            //    new Values_GridView()
            //    {
            //       Cell = "1", Field = "2", Title = "444", Value_K = 2.3, Value_P = 1.3
            //    },
            //    new Values_GridView()
            //    {
            //       Cell = "4", Field = "2", Title = "444", Value_K = 2.3, Value_P = 1.3
            //    }
            //};

            // nie no, super funkcja xD
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Scenario
        void ScenarioInit()
        {
            GUI_Scenario_ScenariosTree.Items.Clear();

            List<Scenario2> subScenario = new List<Scenario2>();
            foreach (Scenario2 sa in Scenario2List)
            {
                subScenario.Add(sa);
            }

            List<string> scenarioList = new List<string>();
            foreach (Scenario2 sa in subScenario)
            {
                if (!scenarioList.Contains(sa.Scenario))
                {
                    scenarioList.Add(sa.Scenario);
                }
            }

            foreach (string str in scenarioList)
            {
                TreeViewItem item = new TreeViewItem() { Header = str };
                List<TreeViewItem> items = FindAllForScenario(subScenario, str);
                foreach (TreeViewItem tvi in items)
                {
                    item.Items.Add(tvi);
                }
                GUI_Scenario_ScenariosTree.Items.Add(item);
            }
        }

        private void GUI_Scenario_ScenariosTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            foreach (TreeViewItem item in GUI_Scenario_ScenariosTree.Items)
            {
                foreach (TreeViewItem item2 in item.Items)
                {
                    foreach (TreeViewItem item3 in item2.Items)
                    {
                        if (item3 == (sender as TreeView).SelectedItem)
                        {
                            _Values_Availability = new ObservableCollection<Values_Availability>();
                            GUI_Scenario_IncludedCellsInScenarioTree.Items.Clear();
                            foreach (Scenario scen in algorithm.GetScenarioData())
                            {
                                if (scen.Description == (string)item3.Header)
                                {
                                    _Values_Availability.Add(new Values_Availability()
                                    {
                                        AuditSection = scen.ID,
                                        Value_Pr = scen.Probability,
                                        Value_POW = scen.Risk,
                                        Value_Sk = scen.Consequence
                                    });

                                    GUI_Scenario_IncludedCellsInScenarioTree.Items.Add(new TreeViewItem() { Header = scen.ID });
                                    ((ColumnSeries)GUI_Scenario_Chart.Series[0]).ItemsSource =
                                        new KeyValuePair<string, int>[]{
                                            new KeyValuePair<string, int>("Pr", scen.Probability),
                                            new KeyValuePair<string, int>("Pow", scen.Risk),
                                            new KeyValuePair<string, int>("Sk", scen.Consequence) };
                                }
                            }
                            GUI_Scenario_AvailabilityGrid.ItemsSource = _Values_Availability;
                            GUI_Scenario_AvailabilityGrid.Items.Refresh();
                            //PropertyChanged(this, new PropertyChangedEventArgs("GUI_Scenario_AvailabilityGrid"));
                            //NotifyPropertyChanged("_Values_Availability");
                        }
                    }
                }
            }
        }


        #endregion

        private void TabItem_MouseMove(object sender, MouseEventArgs e)
        {
            Refresh_Risks_Chart();
        }

        private void GUI_RiskAssessment_WeightGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (RiskAssessment_ReloadGrid)
            {
                InitializeGUI_RiskAssessment_WeightGrid();
                NotifyPropertyChanged("_Values_RiskAssessment");
                RiskAssessment_ReloadGrid = false;
            }
        }

        private void OpenZabbixLogonWindow(object sender, RoutedEventArgs e)
        {
            new ZabbixLogon().Show();
        }
    }
}