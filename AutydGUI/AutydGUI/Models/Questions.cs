﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Controls;

namespace AutydGUI.Models
{
    public enum Answer
    {
        Tak,
        Nie,
        [Description("Brak odpowiedzi")] BrakOdp,
        [Description("Bez przedmiotowo")] Bezprzedmiotowo

    }

    public class Questions : TreeViewItem
    {
        public string ID { get; set; }
        public string Question { get; set; }
        public Answer QuestionAnswer { get; set; }
        public string Comment { get; set; }
        public string Support { get; set; }
        public string Wage { get; set; }
        public string PMax { get; set; }
        public string PMin { get; set; }

        public static Questions Copy(Questions questions)
        {
            return new Questions
            {
                ID = questions.ID,
                Question = questions.Question,
                QuestionAnswer = questions.QuestionAnswer,
                Comment = questions.Comment,
                Support = questions.Support,
                Wage = questions.Wage,
                PMax = questions.PMax,
                PMin = questions.PMin
            };
        }
    }

    class Domain : TreeViewItem
    {
        public string Title { get; set; }
        public string ID { get; set; }
        public List<Service> ListOfServices { get; set; }
        public static Domain Copy(Domain domain)
        {
            return new Domain
            {
                Title = domain.Title,
                ID = domain.ID,
                ListOfServices = new List<Service>(domain.ListOfServices.ConvertAll(Service.Copy))
            };
        }
    }

    class Service : TreeViewItem
    {
        public string Title {get; set;}
        public string ID { get; set; }
        public List<SubService> ListOfSubServices { get; set; }

        public static Service Copy(Service service)
        {
            return new Service
            {
                Title = service.Title,
                ID = service.ID,
                ListOfSubServices = new List<SubService>(service.ListOfSubServices.ConvertAll(SubService.Copy))
            };
        }
    }

    class SubService : TreeViewItem
    {
        public string Title { get; set; }
        public string ID { get; set; }
        public double averageValue { get; set; }
        public List<Questions> ListOfQuestions { get; set; }

        public static SubService Copy(SubService subService)
        {
            return new SubService
            {
                Title = subService.Title,
                ID = subService.ID,
                averageValue = subService.averageValue,
                ListOfQuestions = new List<Questions>(subService.ListOfQuestions.ConvertAll(Questions.Copy))
            };
        }
    }

}
