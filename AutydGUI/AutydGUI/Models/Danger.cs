﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    public class Danger
    {
        public string Incident { get; set; }
        public string Description { get; set; }
        public string DangerValue { get; set; }

        public static Danger Copy(Danger danger)
        {
            return new Danger
            {
                Incident = danger.Incident,
                Description = danger.Description,
                DangerValue = danger.DangerValue,
            };
        }
    }
}
