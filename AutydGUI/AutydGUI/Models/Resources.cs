﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    public class Resources
    {
        public string ID { get; set; }
        public string Description { get; set; }
        public string ResourcesValue { get; set; }
        public string AvailabilityConsequence { get; set; }
        public string IntegrityConsequence { get; set; }
        public string PrivacyConsequence { get; set; } 
        public string Comment { get; set; }
    }

    public class ResourcesType
    {
        public string Title { get; set; }
        public List<Resources> ListOfResources { get; set; }
    }
}