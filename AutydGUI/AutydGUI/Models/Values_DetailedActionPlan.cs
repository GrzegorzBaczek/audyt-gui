﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    class Values_DetailedActionPlan
    {
        public string Code { get; set; }
        public string ServiceID { get; set; }
        public string AuditSection { get; set; }
        public double InitialValue { get; set; }
        public double FinalValue { get; set; }
        public double DirectCost { get; set; }
        public double IndirectCost { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
