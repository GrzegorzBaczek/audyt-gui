﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    public class Values_ServiceImprovement
    {
        public double CurrentIndex { get; set; }
        public string ServiceID { get; set; }
        public string Name { get; set; }
        public string AuditSection { get; set; }
    }
}
