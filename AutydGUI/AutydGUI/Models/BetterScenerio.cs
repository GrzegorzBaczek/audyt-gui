﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    class BetterScenerio
    {
        public string Scenerio { get; set; }
        public string Description { get; set; }
        public string DisruptiveActions { get; set; }
        public string PreventiveActions { get; set; }
        public string ProtectiveActions { get; set; }
        public string SoothingActions { get; set; }
        public string ReproductiveActions { get; set; }
        public string ConsequenceCriteria { get; set; }
        public string CauseCriteria { get; set; }
        public string Resource { get; set; }
        public string Danger { get; set; }
        public int Probability { get; set; }
        public int EstimatedConsequence { get; set; }
        public int Consequence { get; set; }
        public int Risk { get; set; }
    }
}
