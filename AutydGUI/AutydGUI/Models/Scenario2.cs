﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    class Scenario2
    {
        public string Scenario { get; set; }
        public string Cause { get; set; }
        public string Description { get; set; }
        public string CauseCriteria { get; set; }
        public string Danger { get; set; }
        public string Resource { get; set; }
    }
}
