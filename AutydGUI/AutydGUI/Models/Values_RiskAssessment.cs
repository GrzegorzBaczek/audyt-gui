﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    public class Values_RiskAssessment
    {
        public string Group { get; set; }
        public string ScenarioGroupName { get; set; }
        public int InitialWeight { get; set; }
        public int FinalWieght { get; set; }
    }
}
