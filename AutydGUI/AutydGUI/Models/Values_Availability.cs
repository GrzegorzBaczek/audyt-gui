﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    public class Values_Availability
    {
        public string AuditSection { get; set; }
        public int Value_Pr { get; set; }
        public int Value_Sk { get; set; }
        public int Value_POW { get; set; }
    }
}
