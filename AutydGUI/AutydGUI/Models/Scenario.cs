﻿namespace AutydGUI.Models
{
    public class Scenario
    {
        public string ID { get; set; }
        public string Description { get; set; }
        public string DisruptiveActions { get; set; } 
        public string PreventiveActions { get; set; } 
        public string ProtectiveActions { get; set; } 
        public string SoothingActions { get; set; } 
        public string ReproductiveActions { get; set; } 
        public string ConsequenceCriteria { get; set; }
        public string CauseCriteria { get; set; }
        public string Resource { get; set; }
        public string Danger { get; set; }
        public int Probability { get; set; }
        public int EstimatedConsequence { get; set; }
        public int Consequence { get; set; }
        public int Risk { get; set; }

        public static Scenario Copy(Scenario scenario)
        {
            return new Scenario
            {
                ID = scenario.ID,
                Description = scenario.Description,
                DisruptiveActions = scenario.DisruptiveActions,
                PreventiveActions = scenario.PreventiveActions,
                ProtectiveActions = scenario.ProtectiveActions,
                SoothingActions = scenario.SoothingActions,
                ReproductiveActions = scenario.ReproductiveActions,
                ConsequenceCriteria = scenario.ConsequenceCriteria,
                CauseCriteria = scenario.CauseCriteria,
                Resource = scenario.Resource,
                Danger = scenario.Danger,
                Probability = scenario.Probability,
                EstimatedConsequence = scenario.EstimatedConsequence,
                Consequence = scenario.Consequence,
                Risk = scenario.Risk
            };
        }
    }
}
