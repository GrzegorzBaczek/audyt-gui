﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutydGUI.Models
{
    public class Values_GridView
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public double Value_P { get; set; }
        public double Value_K { get; set; }
    }
}
