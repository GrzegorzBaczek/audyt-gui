﻿using System;
using System.Windows;
using System.Windows.Controls;
using Evirth.Zabbix;

namespace AutydGUI
{
    /// <summary>
    /// Interaction logic for ZabbixLogon.xaml
    /// </summary>
    public partial class ZabbixLogon : Window
    {
        public string ZabbixApiUrl { get; set; } = "http://127.0.0.1/zabbix/api_jsonrpc.php";
        public string ZabbixLogin { get; set; } = "admin";
        public string ZabbixPassword { get; set; } = "zabbix";

        public ZabbixLogon()
        {
            InitializeComponent();
            UrlTextBox.Text = ZabbixApiUrl;
            LoginTextBox.Text = ZabbixLogin;
            PasswordBox.Password = ZabbixPassword;
        }

        private void UrlTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var u = (sender as TextBox)?.Text;
            if (u != null)
            {
                ZabbixApiUrl = u.Trim();
            }
        }

        private void LoginTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var l = (sender as TextBox)?.Text;
            if (l != null)
            {
                ZabbixLogin = l.Trim();
            }
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var p = (sender as PasswordBox)?.Password;
            if (p != null)
            {
                ZabbixPassword = p.Trim();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainWindow.ZabbixClient = new ZabbixClient(ZabbixApiUrl, ZabbixLogin, ZabbixPassword);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
